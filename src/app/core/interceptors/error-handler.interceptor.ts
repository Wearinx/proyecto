import { HttpErrorResponse, HttpInterceptorFn } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/internal/operators/catchError';

export const errorHandlerInterceptor: HttpInterceptorFn = (req, next) => {
  return next(req).pipe(catchError((error: HttpErrorResponse) => {
    let ErrorMessage = "";

    if(error.error instanceof ErrorEvent){
      ErrorMessage = `Error: ${error.error.message}`;
    } else {
      ErrorMessage = `Error code: ${error.status}, message: ${error.message}`;
    }

    return throwError(() => ErrorMessage);

  } ))
  
};

