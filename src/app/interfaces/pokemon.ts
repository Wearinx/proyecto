export interface Pokemon {
    name: String;
    url: string;
}

export interface PokemonResults {
    count: number;
    next: String;
    previous?: String;
    results: Pokemon[];
}